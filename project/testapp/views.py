
from django.http import Http404,HttpResponse
from django.template import Context
from django.template.loader import get_template
from django.shortcuts import render
from django.views.generic import TemplateView

from testapp.models import  Author, Book
from testapp.actions import get_books,get_author,get_list

class HomeView(TemplateView):
	template_name ='testapp/home1.html'
	#template_name ='testapp/home2.html'
	#template_name ='testapp/home3.html'
	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)

		context['author_name'] = get_author()
			

		context['books'] = get_books()
		context['list'] = get_list()

		return context





